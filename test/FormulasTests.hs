{-# LANGUAGE TemplateHaskell #-}
module Main where

import FormulasInstances
import Formulas
import Test.QuickCheck (quickCheckAll, quickCheck)

prop_showshowB' :: Formula -> Bool
prop_showshowB' formula = show formula == showB' formula

prop_disjoin_disjoinB :: [Formula] -> Bool
prop_disjoin_disjoinB xs =   (disjoinB xs == disjoin xs)
                          || (disjoinB filtered == disjoin filtered)
                          || (elem TrueF xs && disjoin xs == TrueF)
                          where filtered = filter (/=FalseF) xs

prop_disjoin_trues :: [Formula] -> Bool
prop_disjoin_trues xs = (disjoin xs == TrueF) == elem TrueF xs

prop_disjoin_falses :: [Formula] -> Bool
prop_disjoin_falses xs = disjoin xs == disjoin filtered
                          where filtered = filter (/=FalseF) xs

-- return [] is een vuile hack voor ghc 7.8
return []
runTests :: IO Bool
runTests = $quickCheckAll

main :: IO Bool
main = runTests
