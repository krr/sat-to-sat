module FormulasInstances where

import Formulas
import Test.QuickCheck.Arbitrary
import Test.QuickCheck.Gen

instance Arbitrary Var where
  -- arbitrary = Var <$> listOf (elements ['a','b','c','d'])
  arbitrary = Var <$> elements ["a","b","c","d"]

instance Arbitrary BoolOp where
  arbitrary = elements [C, D]

instance Arbitrary QuantOp where
  arbitrary = elements [A, E]

instance Arbitrary Formula where
  arbitrary =
    oneof [
            Atom <$> arbitrary <*> elements [[]],
            -- Atom <$> arbitrary <*> arbitrary,
            Bool <$> arbitrary <*> arbitrary <*> arbitrary,
            Neg <$> arbitrary,
            Quant <$> arbitrary <*> arbitrary <*> arbitrary,
            elements [TrueF, FalseF]]