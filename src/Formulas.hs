module Formulas where

import Data.Char
import Data.List




-----------------------------------
--   Data structure
-----------------------------------


sq :: String -> String
sq s@[_]                     = s
sq ('"':s)  | last s == '"'  = init s
            | otherwise      = s
sq ('\'':s) | last s == '\'' = init s
            | otherwise      = s
sq s = s


data Var = Var String
  deriving (Eq, Ord)

instance Show Var where
    show (Var s) = sq s

-- conjunction or disjunction
data BoolOp = C | D
         deriving (Show, Eq)

-- Quant A or Quant E
data QuantOp = A | E
         deriving (Show, Eq)

data Formula = Atom Var [Var]
             | Bool BoolOp Formula Formula
             | Neg Formula
             | Quant QuantOp Var Formula
             | TrueF
             | FalseF
    deriving (Eq)



instance Show Formula where
  show (Atom a l)    = show a ++ "(" ++ intercalate ", " (map show l) ++ ")"
  -- Conjunctions put brackets around disjunctions, quantifications, and const
  show (Bool C f g)  = show' f ++ " & " ++ show' g
    where show' x@(Bool D _ _)  = showbrac x
          show' x@Quant{} = showbrac x
          show' x@TrueF = showbrac x
          show' x@FalseF = showbrac x
          show' x = show x
  -- Disjunctions put brackets around conjunctions, quantifications, and const
  show (Bool D f g)  = show' f ++ " | " ++ show' g
    where show' x@(Bool C _ _)  = showbrac x
          show' x@Quant{} = showbrac x
          show' x@TrueF = showbrac x
          show' x@FalseF = showbrac x
          show' x = show x
  -- Negations put brackets around everything except Atoms
  show (Neg f)       = "~ " ++ show' f
    where show' x@(Atom _ _) = show x
          show' x = showbrac x
  show (Quant A v f) = "! " ++ show v ++ ": " ++ show f
  show (Quant E v f) = "? " ++ show v ++ ": " ++ show f
  show TrueF = "true"
  show FalseF = "false"

showB' :: Formula -> String
showB' (Atom a l ) = show a ++ "(" ++ intercalate ", " (map show l) ++ ")"
showB' (Bool C f g) = showbrac_con f ++ " & " ++ showbrac_con g
showB' (Bool D f g) = showbrac_dis f ++ " | " ++ showbrac_dis g
showB' (Neg f) = "~ " ++ showbrac_neg f
showB' (Quant A v f) = "! " ++ show v ++ ": " ++ showbrac_quant f
showB' (Quant E v f) = "? " ++ show v ++ ": " ++ showbrac_quant f
showB' TrueF = "true"
showB' FalseF = "false"

showbrac_con (Bool C f g) = show (Bool C f g)
showbrac_con f = showbrac_bool f

showbrac_dis (Bool D f g) = show (Bool D f g)
showbrac_dis f = showbrac_bool f

showbrac_bool (Atom a l ) = show (Atom a l )
showbrac_bool (Neg f) = show (Neg f)
showbrac_bool f = showbrac f

showbrac_neg (Atom a l ) = show (Atom a l )
showbrac_neg f = showbrac f

showbrac_quant (Quant A v f) = show (Quant A v f)
showbrac_quant (Quant E v f) = show (Quant E v f)
showbrac_quant f = show f

showbrac f = "( " ++ show f ++ " )"






-----------------------------------
--   Operations
-----------------------------------


implies :: Formula -> Formula -> Formula
implies a = Bool D (negation a)

equiv :: Formula -> Formula -> Formula
equiv a b = Bool C (implies a b) (implies b a)

conjoin :: [Formula] -> Formula
conjoin = foldr (>>>=) TrueF
  where
    TrueF  >>>= t      = t
    FalseF >>>= _      = FalseF
    f      >>>= TrueF  = f
    _      >>>= FalseF = FalseF
    f      >>>= g      = Bool C f g

disjoin :: [Formula] -> Formula
disjoin = foldr (>>>=) FalseF
  where
    TrueF  >>>= _       = TrueF
    FalseF >>>= f       = f
    f      >>>= TrueF   = TrueF
    f      >>>= FalseF  = f
    f      >>>= g       = Bool D f g

-- Deze reduceert bijv inoptimaal: [Atom (Var "a") [], FalseF, FalseF]
disjoinB [] = FalseF
disjoinB [f] = f
disjoinB (TrueF:t) = TrueF
disjoinB (FalseF:t) = disjoinB t
disjoinB (_:[TrueF]) = TrueF
disjoinB (f:[FalseF]) = f
disjoinB (h:t) = Bool D h (disjoinB t)

quantify :: QuantOp -> [Var] -> Formula -> Formula
quantify _ [] f = f
quantify q (h:t) f = Quant q h $ quantify q t f

negation :: Formula -> Formula
negation (Neg f) = f
negation f = Neg f

isQuantification :: Formula -> Bool
isQuantification Quant{} = True
isQuantification _ = False

------------------------------------
--       Analysis
------------------------------------

isFOVar :: Var -> Bool
isFOVar (Var s) = isUpper $ head s

isInputVar :: Var -> Bool
isInputVar (Var s) = isPrefixOf "in_" s || isPrefixOf "In_" s
