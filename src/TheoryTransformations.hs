module TheoryTransformations where

import Formulas
import Control.Monad.State
import Control.Lens
import Data.Tuple
import TransformationData

normalise :: Formula -> Formula
normalise =  --toAssoc .
    normaliseSameName . ensureUniqueIDs

normaliseSameName =
    pullForAll . pullSOQuant . quantNegNormalForm

----------------------------------
-- Pull ForAll quantifications (TODO: and conjunction) out of disjunctions
----------------------------------
pullForAll :: Formula -> Formula
pullForAll (Bool b f g) = pullForAllNonRecursive $ Bool b (pullForAll f) (pullForAll g)
pullForAll (Neg f) = Neg $ pullForAll f
pullForAll (Quant q v f) = Quant q v $ pullForAll f
pullForAll f = f

pullForAllNonRecursive :: Formula -> Formula
pullForAllNonRecursive (Bool D (Quant A v f) g) = Quant A v $ pullForAllNonRecursive $ Bool D f g
pullForAllNonRecursive (Bool D f (Quant A v g)) = Quant A v $ pullForAllNonRecursive $ Bool D f g
pullForAllNonRecursive f = f


----------------------------------
-- Pull SO quantifications out
----------------------------------
-- Pulls out existential second order quantifications
-- reason: groups quantifiers that together generate a new level
-- Assumes formula is alrady in QNNF (see below)
pullSOQuant :: Formula -> Formula
pullSOQuant (Bool c f g) = pullSOQuantNonRecursive $ Bool c (pullSOQuant f) (pullSOQuant g)
pullSOQuant (Neg f) = pullSOQuantNonRecursive $ Neg (pullSOQuant f)
pullSOQuant (Quant q v f) = pullSOQuantNonRecursive $ Quant q v (pullSOQuant f)
pullSOQuant f = f



pullSOQuantNonRecursive :: Formula -> Formula
pullSOQuantNonRecursive (Bool b (Quant E v f) g )
    | isFOVar v = Bool b (Quant E v f) g
    | otherwise = Quant E v $ pullSOQuantNonRecursive $ Bool b f g
pullSOQuantNonRecursive (Bool b g (Quant E v f)  )
    | isFOVar v = Bool b (Quant E v f) g
    | otherwise = Quant E v $ pullSOQuantNonRecursive $ Bool b f g
pullSOQuantNonRecursive (Quant E v ( Quant E w g) )
    | isFOVar v && not (isFOVar w)   = Quant E w $ pullSOQuantNonRecursive $ Quant E v g
    | otherwise = Quant E v ( Quant E w g)
pullSOQuantNonRecursive f = f


-----------------------------------------
--   Exploits associativity to make left associaciative.
-----------------------------------------

--moves conjunctions and disjunctions to be of the form (a & ( b & ( ... )))
toAssoc :: Formula -> Formula
toAssoc (Bool C (Bool C a b) c) = toAssoc $ Bool C a (Bool C b c)
toAssoc (Bool D (Bool D a b) c) = toAssoc $ Bool D a (Bool D b c)

toAssoc (Atom a t) = Atom a t
toAssoc (Bool C a b) = Bool C (toAssoc a) (toAssoc b)
toAssoc (Bool D a b) = Bool D (toAssoc a) (toAssoc b)
toAssoc (Neg f) = Neg $ toAssoc f
toAssoc (Quant A v f) = Quant A v $ toAssoc f
toAssoc (Quant E v f) = Quant E v $ toAssoc f
toAssoc TrueF = TrueF
toAssoc FalseF = FalseF


-----------------------------------------
-- Quantifiers and negation Normal Form (QNNF)
-----------------------------------------
--maps a formula to its normal form where:
-- SO quantifications only of the form: exists or not exists ...
-- negations only in that place or in front of atoms
quantNegNormalForm :: Formula -> Formula
quantNegNormalForm (Neg (Atom x y)) = Neg $ Atom x y
quantNegNormalForm (Neg (Bool C a b)) = Bool D (quantNegNormalForm $ Neg a) (quantNegNormalForm $ Neg b)
quantNegNormalForm (Neg (Bool D a b)) = Bool C (quantNegNormalForm $ Neg a) (quantNegNormalForm $ Neg b)
quantNegNormalForm (Neg (Neg f)) = quantNegNormalForm f
quantNegNormalForm (Neg TrueF) = FalseF
quantNegNormalForm (Neg FalseF) = TrueF
quantNegNormalForm (Neg (Quant E v f))
          | isFOVar v = Quant A v $ quantNegNormalForm (Neg f)
          | otherwise = Neg (Quant E v (quantNegNormalForm f) )
quantNegNormalForm (Neg (Quant A v f)) = Quant E v $  quantNegNormalForm (Neg f)

quantNegNormalForm (Bool b f g) = Bool b (quantNegNormalForm f) (quantNegNormalForm g)
quantNegNormalForm (Atom x y) = Atom x y
quantNegNormalForm (Quant A v f)
          | isFOVar v = Quant A v (quantNegNormalForm f)
          | otherwise = Neg $ Quant E v $ quantNegNormalForm $  Neg f
quantNegNormalForm (Quant E v f) = Quant E v $ quantNegNormalForm f
quantNegNormalForm TrueF = TrueF
quantNegNormalForm FalseF = FalseF

-----------------------------------
--    Unique names
-----------------------------------
-- Ensures that all quantifiers quantify over a unique variable

type IDManagement = State Int

generateID :: IDManagement Int
generateID = id <+= 1

runIDManagement :: IDManagement a -> a
runIDManagement m = evalState m 0

ensureUniqueIDs :: Formula -> Formula
ensureUniqueIDs f = runIDManagement $ ensureUniqueIDsInternal f


ensureUniqueIDsInternal :: Formula -> IDManagement Formula
ensureUniqueIDsInternal f =  changeNames f id

changeNames :: Formula -> (String -> String) -> IDManagement Formula
changeNames (Atom pred args) strmap = return $ Atom (changeVarName strmap pred) (map (changeVarName strmap) args)
changeNames (Bool b f g) strmap = Bool b <$>
    changeNames f strmap <*>
    changeNames g strmap
changeNames (Neg f) strmap = Neg <$>
    changeNames f strmap

changeNames (Quant q (Var s) f) strmap = do
      nid <- generateID
      let prefix = if isFOVar (Var s) then "FO_" else "so_"
      let str = prefix ++ show nid ++ "_" ++ s
      let newmap x = if x == s then str else strmap x
      Quant q (Var str) <$> changeNames f newmap


changeNames TrueF _ = return TrueF
changeNames FalseF _ = return FalseF

changeVarName :: (String -> String) -> Var -> Var
changeVarName f (Var a)  = Var $  f a

-----------------------------------
--    Replace Var By Var
-----------------------------------
-- Replaces variables in internal quantifications by their upper, respectively lower bound
-- Depending on the number of negations they occur in
-- Even number of negations: replace p by p_{pt}    -- In Shahabs terminology: this is UPPER BOUND
-- Odd number of negation: replace p by p_{ct}      -- In Shahabs terminology: this is LOWER BOUND

-- ReplaceState
-- [Var]: List of variables quantified in current level.
-- Var -> Var: List of current replacements
-- Var -> Var: List of inverse replacements (under other number of negations modulo 2)


replaceSOVarByBounds :: Formula -> Formula
replaceSOVarByBounds = runReplaceStateManagement . replaceSOVarByBoundsInternal

type ReplaceStateManagement = State StackedReplaceState

swapNegation :: ReplaceStateManagement ()
swapNegation = current.repl %= swap

popQuantifiedVar :: ReplaceStateManagement ()
popQuantifiedVar = current.vars %= tail

addQuantifiedVar :: Var -> ReplaceStateManagement ()
addQuantifiedVar v = current.vars %= (v:)

locally :: ReplaceStateManagement () -> ReplaceStateManagement () -> ReplaceStateManagement a -> ReplaceStateManagement a
locally f fi a = do
    f
    r <- a
    fi
    return r

certainlyTruePred :: Var -> Var
certainlyTruePred (Var x) = Var $ x ++ "_ct"

possiblyTruePred  :: Var -> Var
possiblyTruePred  (Var x) = Var $ x ++ "_pt"

--Performs the negation swapping itself!
nextLevel :: ReplaceStateManagement ()
nextLevel = do
    swapNegation
    cur@(RS vars (curRepl,curOther)) <- use current
    let newrepl x = if x `elem` vars then certainlyTruePred x else curRepl x
    let newother x = if x `elem` vars then possiblyTruePred x else curOther x
    current .= RS [] (newrepl, newother)
    backup %= (cur :)
    current %= ((repl . _1) .~ newrepl)

--Performs the negation swapping itself!

popLevel :: ReplaceStateManagement ()
popLevel = do
    newCur <- head <$> use backup
    current .= newCur
    backup %= tail
    swapNegation

getMappedVar :: Var -> ReplaceStateManagement Var
getMappedVar v = (&) v <$> use (current. repl . _1)

runReplaceStateManagement :: ReplaceStateManagement a -> a
runReplaceStateManagement m = evalState m (SRS (RS [] (id, id)) [])


replaceSOVarByBoundsInternal :: Formula -> ReplaceStateManagement Formula
replaceSOVarByBoundsInternal (Atom a l) = Atom <$> getMappedVar a <*> return l
replaceSOVarByBoundsInternal (Neg (Quant E v f))
    | not (isFOVar v) = Neg <$>
        locally nextLevel popLevel (replaceSOVarByBoundsInternal (Quant E v f))
replaceSOVarByBoundsInternal (Neg f) = Neg <$>
        locally swapNegation swapNegation (replaceSOVarByBoundsInternal f)
replaceSOVarByBoundsInternal (Quant E v f)
    | not (isFOVar v) = Quant E v <$>
        locally (addQuantifiedVar v) popQuantifiedVar (replaceSOVarByBoundsInternal f)
    | otherwise = Quant E v <$> replaceSOVarByBoundsInternal f
replaceSOVarByBoundsInternal (Quant A v f)
    | not (isFOVar v) = error $ "Unexpected quantification in " ++ show (Quant A v f)
    | otherwise = Quant A v <$> replaceSOVarByBoundsInternal f
replaceSOVarByBoundsInternal TrueF = return TrueF
replaceSOVarByBoundsInternal FalseF = return FalseF
replaceSOVarByBoundsInternal (Bool b f g) = Bool b <$>
            replaceSOVarByBoundsInternal f <*>
            replaceSOVarByBoundsInternal g
