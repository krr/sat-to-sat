module PrintGringo where

import Formulas
import System.FilePath
import Control.Monad.State
import TheoryTransformations
import Collect
import Data.List

-- Prints a given formula as files in the directory specified by the string
printGringo :: Formula -> FilePath -> IO ()
printGringo f dir = do
    let fTrans = replaceSOVarByBounds f
    let result = getGringoStrings "" fTrans
    writeToFiles result dir
    writeRunFile dir

writeRunFile :: FilePath -> IO()
writeRunFile dir = do
    let file = joinPath [dir,  "run.sh"]
    let str = concat [
            "#!/bin/bash\n",
            "#Shell script generated by so-sat2sat. Should be ran with first argument: the s2s linker; second argument: an instance file. \n \n",
            "tmpdir=`mktemp -d`\n",
            "cd $tmpdir\n",
            "cp ",dir,"/* .\n",
            "bash trans.sh $1 $2\n",
            "rm $tmpdir -r"]
    writeFile file str


data MyFile = MyFile {name :: String, content :: String}
   deriving (Show)

writeToFile :: MyFile -> FilePath -> IO ()
writeToFile (MyFile name content) dir = writeFile (joinPath [dir,  name]) content

writeToFiles ::  [MyFile]-> FilePath -> IO()
writeToFiles [] dir = return ()
writeToFiles (h: t) dir = do
    writeToFile h dir
    writeToFiles t dir


addContentIfHolds :: (String -> Bool) -> String -> MyFile -> MyFile
addContentIfHolds f extra (MyFile name content)
    | f name = MyFile name (content ++ extra)
    | otherwise = MyFile name content

-- addContentIf name extra file equals file if name does not coincide with file's name.
-- Otherwise, this equals the file equal to file except that the extra content is added
addContentIf :: String -> String -> MyFile -> MyFile
addContentIf s extra (MyFile name content)
    | s == name = MyFile s (content ++ extra)
    | otherwise = MyFile name content

addContentIfFront :: String -> String -> MyFile -> MyFile
addContentIfFront s extra (MyFile name content)
    | s == name = MyFile s (extra ++ content)
    | otherwise = MyFile name content


-- Mystate
-- ID used for file/Tseiting generation
-- List of files with content
-- String: current file writing to
-- [String]: previous stack of files
-- [[Var]]: List of lists of vars: [l1,l2,l3] means: l1 are the vars quantified in the current level, l2 in the above level, ...
-- String: the directory in which all files are placed
type MyState = (Int, [MyFile], String, [String], [[Var]], String )
type MyStateManagement = State MyState

getSOTseitin :: MyStateManagement Var
getSOTseitin = do
    (n, currStrings, file ,l, vars, dir) <- get
    put ( n+1, currStrings, file, l, vars, dir)
    return $ Var ("so_tseitin_" ++ show n)

startDeeperLevel:: MyStateManagement String
startDeeperLevel = do
    f <- generateFileName
    (n, currStrings, file, l, vars, dir) <- get
    put (n, currStrings, f, file:l, []:vars, dir)
    return f


previousLevel:: MyStateManagement ()
previousLevel = do
    (_, _, deeplevel, toplevel:_, h_in :vars_in, dir) <- get
    unless (null h_in) (error "ERROR: Internal error in level popping")
    --When returning to the previous level: add the command to link the old level to the new level
    unless (null vars_in) (
           addToScript $ getLinkScript toplevel deeplevel $ createUpperLowerMapping vars_in)
    (n, currStrings, file, prev:rest, h:vars, dir) <- get
    put (n, currStrings, prev, rest, vars, dir)

atHighestLevel :: MyStateManagement Bool
atHighestLevel = do
    (_, _, _, h:t, _, _) <- get
    return (null t)


addQuantifiedVariable :: Var -> MyStateManagement ()
addQuantifiedVariable v = do
    (n, currStrings, file, l, h:vars, dir) <- get
    put (n, currStrings, file, l, (v:h):vars, dir)

popQuantifiedVariable :: MyStateManagement ()
popQuantifiedVariable = do
    (n, currStrings, file, l, (v:h):vars, dir) <- get
    put (n, currStrings, file, l, h:vars, dir)

addToScript :: String -> MyStateManagement ()
addToScript content =    do
    (n, currStrings, name, l, vars, dir) <- get
    let newStrings = map (addContentIf (scriptName dir) content) currStrings
    put (n, newStrings, name, l, vars, dir)
    return ()

scriptName :: FilePath -> FilePath
scriptName dir = joinPath [dir,  "trans.sh"]


--Private:
generateFileName :: MyStateManagement String
generateFileName = do
    (n, currStrings, file, l, vars, dir) <- get
    let s = show n
    let padded = replicate (4-length s) '0' ++ s
    let result = joinPath [dir, padded ++ ".lp"]
    put ( n+1,  MyFile result "" : currStrings, file, l, vars, dir)
    return result

-- adds the content to the file in the state with the given name
addContent :: String ->  MyStateManagement ()
addContent content = do
    (n, currStrings, name, l, vars, dir) <- get
    let newStrings = map (addContentIf name content) currStrings
    put (n, newStrings, name, l, vars, dir)
    return ()

runMyStateManagement :: FilePath -> MyStateManagement a -> MyState
runMyStateManagement dir m = execState m (0 , [MyFile (scriptName dir) "" ], "", [], [], dir)

getGringoStrings :: FilePath -> Formula -> [MyFile]
getGringoStrings dir f = do
    let (_, l,_ ,_ ,_,_) = runMyStateManagement dir $ executegringo f
    l

executegringo :: Formula -> MyStateManagement ()
executegringo f = do
    _ <- getGringoStringsInternal f
    finishScript
    addHideStatements f

--Adds hide statements to all logic programs.
addHideStatements :: Formula ->  MyStateManagement ()
addHideStatements f = do
    let l = collectInputPredicates f
    let str = concatMap getHideString l
    (n, myFiles, currFile, x,y, dir) <- get
    let finalFiles = map (addContentIfHolds (Data.List.isSuffixOf ".lp")  str) myFiles
    put (n, finalFiles, currFile, x, y, dir)
getHideString :: (Var, Int) -> String 
getHideString (v, i) = 	"" --concat [ "#hide ", show v, "/", show i, ".\n"] --TODO

--Finishes the script by adding all gringo satgrnd calls etcetera to the script
finishScript :: MyStateManagement ()
finishScript = do
    (n, myFiles, currFile, x,y, dir) <- get
    let head = "#!/bin/bash\n#Shell script generated by so-sat2sat. Should be ran from wihtin the directory in which this file resides with first argument: the s2s linker; second argumetn: an instance file. \n#Generates an extendended dimacs file to be used only by sat2sat (since the comments contain important directions for the solver)\nlinker=$1\ninstance=$2\nemptyfile=`mktemp`\n\n#Compile each of the lp files into a cnf file.\n"
    let newFiles = map (addContentIfFront (scriptName dir) $ concatMap (file2cnf dir) myFiles) myFiles
    let newerFiles = map (addContentIfFront (scriptName dir) head) newFiles
    let finalFiles = map (addContentIf (scriptName dir) "\n#Output the final result\ncat 0000.lp.cnf\n\n") newerFiles
    put (n, finalFiles, currFile, x, y, dir)



file2cnf :: FilePath -> MyFile -> String
file2cnf dir (MyFile name _ )
    | name == scriptName dir = ""
    | otherwise = "cat " ++ name ++ " $instance | gringo5 -o smodels | satgrnd > " ++ name ++ ".cnf\n"    

----------------------------------------------
----Stuff related to creating linking script:
----------------------------------------------

-- returns a string to be used for the linker script that
-- maps the first level quantification to Upper and Lower preds
-- for all higher quantified levels:: maps upper to upper and lower to lower
createUpperLowerMapping :: [[Var]] -> String
createUpperLowerMapping [] = error "ERROR: creating upper-lower mapping on empty list"
createUpperLowerMapping (h:t) = createRealULMapping h ++ concatMap createConstantMapping t

createRealULMapping :: [Var] -> String
createRealULMapping [] = ""
createRealULMapping (h:t) = " -u " ++ show h ++ " " ++ show (possiblyTruePred h) ++ " -l "++ show h ++ " " ++  show (certainlyTruePred h) ++ createRealULMapping t

createConstantMapping :: [Var] -> String
createConstantMapping [] = ""
createConstantMapping (h:t) = " -u " ++ show (possiblyTruePred h) ++ " " ++ show (possiblyTruePred h) ++ " -u "++ show (certainlyTruePred h) ++ " " ++ show (certainlyTruePred h) ++ createConstantMapping t


--Input: topfile, nested file, upperlowermapping
-- Output: entire sh command for linking
getLinkScript :: String -> String -> String -> String
getLinkScript top bot args = do
    let topcnf = top ++ ".cnf"
    let botcnf = bot ++ ".cnf"
    let toptmp = topcnf ++ ".tmp"
    let head = "\n#Add " ++ bot ++ " as a propagator to " ++ top ++ "\n"
    let script1 = "cp " ++ topcnf ++ " " ++ toptmp ++ " \n"
    let script2 = "$linker " ++ toptmp ++ " " ++ botcnf ++ " $emptyfile" ++ args ++ " > " ++ topcnf ++ "\n"
    let script3 = "rm " ++ toptmp ++ "\n"
    head ++ script1 ++ script2 ++ script3

----------------------------------------------
-- Really transforming a formula to its lp representation
----------------------------------------------

getGringoStringsInternal :: Formula -> MyStateManagement ()
getGringoStringsInternal f = do
    _ <- startDeeperLevel
    writeTheory f
    previousLevel

-- First level: Existential SO quantifications
writeTheory :: Formula -> MyStateManagement ()
writeTheory (Quant E v f)
   | isFOVar v = writeTheoryConj (Quant E v f)
   | otherwise = do
        addQuantifiedVariable v
        addContent $ gringoNotifyExists v
        writeTheory f
        popQuantifiedVariable
writeTheory f = do
    addContent gringoTrueFalse
    writeTheoryConj f


-- Second level: (dissolve conjunctions)
writeTheoryConj :: Formula -> MyStateManagement ()
writeTheoryConj  (Bool C f g) = do
    writeTheoryConj f
    writeTheoryConj g
writeTheoryConj (Neg (Quant E v f))
   | isFOVar v = error $ "ERROR: Unexpected negation: " ++ show ( Neg $ Quant E v f )
   | otherwise = do
        newfile <- startDeeperLevel
        writeTheory (Quant E v f)
        previousLevel
        addContent $ gringoUnsatFile newfile
writeTheoryConj f  = writeRule f


--Next level: write some rule
writeRule :: Formula -> MyStateManagement ()
writeRule f = do
    let (vars, conds, remainder ) = doCollectionUniv f
    writeRuleIntern remainder (vars, conds)
    return ()

writeRuleIntern ::   Formula -> ([Var], [Formula]) -> MyStateManagement()
writeRuleIntern (Bool C f g) (vars, conds) = do
    let condition = conjoin conds
    let fnew = quantify A vars $ implies condition f
    let gnew = quantify A vars $ implies condition g
    _ <- writeRule fnew
    writeRule gnew
writeRuleIntern f (vars, conds) = do
    let tailprint = printGringoBody conds
    (headR, triv) <-  compileRuleHead f  (vars, conds)
    let newcontent = if not triv then headR ++ tailprint else ""
    addContent newcontent
    return ()



-- Returns: universally quantified variables, input-conditions on those vars, and remainder
-- Rewrites input to: ! x y z: cond(x,y,z) => remainder (where cond(x,y,z) is an input-conjunction)
doCollectionUniv ::Formula -> ([Var], [Formula], Formula)
doCollectionUniv f = do
    let res = collectUnivVarsAndConditions f
    collectUnivVarsAndConditionsRecursively f res

collectUnivVarsAndConditionsRecursively::  Formula -> ([Var], [Formula], Formula)  -> ([Var], [Formula], Formula)
collectUnivVarsAndConditionsRecursively f (vars, conds, g)
    | f == g = (vars, conds, g)
    | otherwise = do
        let (newvars, newconds, newg) = collectUnivVarsAndConditions g
        collectUnivVarsAndConditionsRecursively g (vars ++ newvars, conds ++ newconds, newg)


collectUnivVarsAndConditions :: Formula -> ([Var], [Formula], Formula)
collectUnivVarsAndConditions (Quant A v f) = do
    let (vars, conds, remainder ) = collectUnivVarsAndConditions f
    (v:vars, conds, remainder)
collectUnivVarsAndConditions f =  collectForAllConditions (normaliseSameName f)

collectForAllConditions :: Formula -> ([Var], [Formula], Formula)
collectForAllConditions (Bool D f g) = do
    let (vars1, conds1, rest1) = collectForAllConditions f
    let (vars2, conds2, rest2) = collectForAllConditions g
    (vars1 ++ vars2, conds1 ++ conds2, disjoin [rest1, rest2])
collectForAllConditions (Atom p l)
    | isInputVar p = ([],[Neg $ Atom p l],FalseF)
collectForAllConditions (Neg (Atom p l) )
    | isInputVar p = ([],[Atom p l],FalseF)
collectForAllConditions f = ([],[],f)



-- Finds the string representation of a rule head (first argument).
-- The second argument is the set of formulas in the body of the rule of this head:
--    this is needed in case we create extra Tseitins internally

compileRuleHead :: Formula -> ([Var], [Formula]) -> MyStateManagement (String, Bool)
compileRuleHead (Bool D f g) conds = do
    (sf, trivf) <- compileRuleHead f conds
    (sg, trivg) <- compileRuleHead g conds
    if trivf || trivg then return (gringoTrue, True) else return (gringoDisjoinHeads [sf, sg], False)
compileRuleHead f conds = compileSingleRuleHead f conds

compileSingleRuleHead :: Formula -> ([Var], [Formula]) -> MyStateManagement (String, Bool)
compileSingleRuleHead f (vars, conds) = do
   let (existvars, existconds, remainder ) = collectExistVarsAndConditions f
   (headPrinted, trivial) <- printAsHeadLit remainder (vars ++ existvars, conds ++ existconds)
   -- TODO specific print method for existconds here
   let condprint = printGringoHeadCondition existconds
   return (headPrinted ++ condprint, trivial)


--Bool says whether or not this is trivially satisfied
printAsHeadLit :: Formula -> ([Var], [Formula]) -> MyStateManagement (String, Bool)
printAsHeadLit TrueF conds = return (gringoTrue, True)
printAsHeadLit FalseF conds = return (gringoFalse, False)
printAsHeadLit (Atom a l) conds = return (printGringoHeadLit $ Atom a l, False)
printAsHeadLit (Neg (Atom a l)) conds = return (printGringoHeadLit $ Neg $ Atom a l, False)
printAsHeadLit f (vars, conds) = do
    newpredname <- getSOTseitin
    let newAtom = Atom newpredname vars
    b <- atHighestLevel
    addEquivalence newAtom f True b (vars, conds)
    return (printGringoHeadLit newAtom, False)


-- Preconditions:
-- -- first formula is a literal.
-- -- second formula is not a literal or trivial formula
-- The two bools indicate which part of the equivalence needs to be added
addEquivalence :: Formula -> Formula -> Bool -> Bool -> ([Var], [Formula]) -> MyStateManagement ()
addEquivalence head (Bool D f g) ifPart onlyIfPart (vars, conds) = do
    -- todo body print
    let body = printGringoBody conds

    when (ifPart && onlyIfPart)
        -- this is an optimisation to only make the tseitins once
        ( do
        --      head => f | g           -head | f | g
        (pf, _) <- printAsHeadLit f (vars, conds)
        (pg, _) <- printAsHeadLit g (vars, conds)
        let negph = printGringoHeadLit $ negation head
        addContent $ gringoDisjoinHeads [negph, pf, pg] ++ body

         --      head <= f | g           head | -f      head | -g
        let ph = printGringoHeadLit head
        let npf = gringoNegateHead pf
        let npg = gringoNegateHead pg
        addContent $ gringoDisjoinHeads [ph, npf] ++ body
        addContent $ gringoDisjoinHeads [ph, npg] ++ body
        )


    --      head => f | g           -head | f | g
    when (ifPart && not onlyIfPart) (do
        (pf, _) <- printAsHeadLit f (vars, conds)
        (pg, _) <- printAsHeadLit g (vars, conds)
        let negph = printGringoHeadLit $ negation head
        addContent $ gringoDisjoinHeads [negph, pf, pg] ++ body
        )
    --      head <= f | g           head | -f      head | -g
    when (onlyIfPart && not ifPart)(do
            let ph = printGringoHeadLit head
            (npf, _) <- printAsHeadLit ( normaliseSameName $ negation f) (vars, conds)
            (npg, _) <- printAsHeadLit ( normaliseSameName $ negation g) (vars, conds)
            addContent $ gringoDisjoinHeads [ph, npf] ++ body
            addContent $ gringoDisjoinHeads [ph, npg] ++ body
        )
addEquivalence head (Bool C f g) ifPart onlyIfPart (vars, conds) = do
    let negF = normaliseSameName $ negation f
    let negG = normaliseSameName $ negation g
    let negH = negation head
    addEquivalence negH (Bool D negF negG) onlyIfPart ifPart (vars, conds)
addEquivalence head (Quant A v f ) ifPart onlyIfPart (vars, conds) = do
    -- !vars: conditions => (p <=> !v:f)

    when ifPart (do
            --FIRST:
            --  !vars, v: (conditions & p) => f)
            let negconds = map negation conds
            let body = disjoin (f: (negation head : conds))
            let newform = quantify A (v:vars) body
            -- reuse smartness for this formula writeRule newform
            writeRule newform
        )
    when onlyIfPart (do
            --SECOND:
            -- !vars: (conditions) => (!v:f) => p)
            -- or: ~p => ? v: ~f
            let negF = normaliseSameName $ negation f
            let rulehead = Quant E v negF
            (negFPrint, triv) <- compileSingleRuleHead rulehead (vars, conds)
            addContent $ gringoDisjoinHeads [printGringoHeadLit head, negFPrint ++ printGringoBody conds ]
        )
addEquivalence head (Quant E v f ) ifPart onlyIfPart  (vars, conds) = do
    let negH = negation head
    let negF = normaliseSameName $ negation (Quant E v f )
    addEquivalence negH negF onlyIfPart ifPart (vars, conds)
addEquivalence f g _ _ _ = error $ "ERROR: Not yet implemented: equivalence printing for " ++ show f ++ " AND " ++ show g



-- Returns: existentially quantified variables, input-conditions on those vars, and remainder
-- Rewrites input to: ? x y z: cond(x,y,z) & remainder (where cond(x,y,z) is an input-conjunction)
collectExistVarsAndConditions :: Formula -> ([Var], [Formula], Formula)
collectExistVarsAndConditions (Quant E v f) = do
    let (vars ,conds ,remainder ) = collectExistVarsAndConditions f
    (v:vars, conds, remainder)
collectExistVarsAndConditions f =  collectExistsConditions (normaliseSameName f)

collectExistsConditions :: Formula -> ([Var], [Formula], Formula)
collectExistsConditions (Bool C f g) = do
    let (vars1, conds1, rest1) = collectExistsConditions f
    let (vars2, conds2, rest2) = collectExistsConditions g
    (vars1++vars2, conds1++ conds2, conjoin [rest1, rest2])
collectExistsConditions (Atom p l)
    | isInputVar p = ([],[Atom p l],TrueF)
    | otherwise = ([],[], Atom p l )
collectExistsConditions (Neg (Atom p l) )
    | isInputVar p = ([],[Neg $ Atom p l],TrueF)
    | otherwise = ([],[], Neg $ Atom p l )
collectExistsConditions f = ([],[],f)


-------------------------------------------------------
-- Gringo specific printing methods
-------------------------------------------------------

--Input: list of literals
printGringoBody :: [Formula] -> String
printGringoBody [] = ".\n"
printGringoBody l = do
    -- conjoin eliminates some trivialities already
    let f = conjoin l
    " :- " ++ printGringoOnlyBody f ++ ".\n"


--Input :: conjunction of literals
printGringoOnlyBody :: Formula -> String
printGringoOnlyBody (Atom p l) = show (Atom p l)
printGringoOnlyBody (Neg f) = "not " ++ printGringoOnlyBody f
printGringoOnlyBody TrueF = gringoTrue
printGringoOnlyBody FalseF = gringoFalse
printGringoOnlyBody (Bool C f g) = printGringoOnlyBody f ++ " , " ++ printGringoOnlyBody g
printGringoOnlyBody f = error $ "ERROR: printing the following formula as gringo body: " ++ show f

-- Prints that an SO variable is existentially quantified
gringoNotifyExists :: Var -> String
gringoNotifyExists v = "% EXISTS " ++ show v ++ ": (" ++ show (certainlyTruePred v) ++ ", " ++ show (possiblyTruePred v) ++ ") = (LOWER, UPPER) \n"

gringoTrue :: String
gringoTrue = "true"

gringoFalse :: String
gringoFalse = "false"

--Prints basic true false info
gringoTrueFalse :: String
gringoTrueFalse = unlines $ map unwords [["-", gringoFalse, ":-", gringoTrue, "."],[gringoTrue, "."]]

--Prints that certain file contains internal propagator
gringoUnsatFile :: String -> String
gringoUnsatFile f = "% UNSAT FILE:" ++ f ++ "\n"

--Disjoins string representations of rule heads
gringoDisjoinHeads :: [String] -> String
gringoDisjoinHeads [] = ""
gringoDisjoinHeads [s] = s
gringoDisjoinHeads (h:t) = h ++ " | " ++ gringoDisjoinHeads t

printGringoHeadCondition :: [Formula] -> String
printGringoHeadCondition [] = ""
printGringoHeadCondition l = " : " ++ intercalate ", " (map printGringoOnlyBody l)

printGringoHeadLit :: Formula -> String
printGringoHeadLit (Atom p l) = show (Atom p l)
printGringoHeadLit (Neg (Atom p l)) = gringoNegateHead $ printGringoOnlyBody (Atom p l)
printGringoHeadLit TrueF = gringoTrue
printGringoHeadLit FalseF = gringoFalse
printGringoHeadLit f = error $ "ERROR: printing the following formula as gringo head literal: " ++ show f

gringoNegateHead :: String -> String
gringoNegateHead (' ':('-':s)) = s
gringoNegateHead s = " - " ++ s
