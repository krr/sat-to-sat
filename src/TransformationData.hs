{-# LANGUAGE TemplateHaskell #-}
module TransformationData where

import Formulas
import Control.Lens.TH

data ReplaceState = RS {_vars :: [Var], _repl :: (Var -> Var, Var -> Var) }
data StackedReplaceState = SRS { _current ::ReplaceState, _backup :: [ReplaceState]}
makeLenses ''ReplaceState
makeLenses ''StackedReplaceState
