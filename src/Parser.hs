module Parser where

import           Formulas
import           PrattParser
import           Text.ParserCombinators.Parsec
import           Text.ParserCombinators.Parsec.Expr
import           Text.ParserCombinators.Parsec.Language
import qualified Text.ParserCombinators.Parsec.Token    as Token

--PARSING

myParser :: Parser Formula
myParser = whiteSpace >> parseMulti

myeofparser :: Parser Formula
myeofparser =  eof >> return TrueF

parseMulti :: Parser Formula
parseMulti = conjoin <$> many formulaParser


formulaParser :: Parser Formula
formulaParser = do
    a <- statement'
    dot
    return a

sequenceOfID = sepBy varparse comma

varparse = Var <$> identifier

statement' :: Parser Formula
statement' =    combinedExpression

atomStmt :: Parser Formula
atomStmt = Atom <$> varparse <*> parens sequenceOfID

existsParser :: Parser Var
existsParser = do
    reservedOp "?"
    v <- varparse
    reservedOp ":"
    return v

forallParser :: Parser Var
forallParser = do
    reservedOp "!"
    v <- varparse
    reservedOp ":"
    return v

combinedExpression :: Parser Formula
combinedExpression = buildPrattParser operators term

term =   parens combinedExpression <|> atomStmt

prefix  p = Prefix  . chainl1 p $ return (.)
chained p = chainl1 p $ return (.)

tilde = reservedOp "~" >> return Neg
quest = Quant E <$> existsParser
bang = Quant A <$> forallParser


operators = [
         [ Prefix tilde]
        ,[Infix  (reservedOp "&" >> return (Bool C     )) AssocLeft]
        ,[ Infix  (reservedOp "|"  >> return (Bool D      )) AssocLeft]
        ,[ Infix  (reservedOp "=>"  >> return implies      ) AssocRight]
        ,[ Infix (reservedOp "<=" >> return (flip implies) ) AssocRight]
        ,[ Infix (reservedOp "<=>" >> return (\ x y -> Bool C (implies x y) (implies y x) ) ) AssocLeft]
        ,[ Prefix quest, Prefix bang ]
         ]


parseFile :: String -> IO Formula
parseFile file =
   do program  <- readFile file
      case parse myParser "" program of
        Left e  -> print e >> fail "parse error"
        Right r -> return r


languageDef =
   emptyDef { Token.commentStart    = "/*"
            , Token.commentEnd      = "*/"
            , Token.commentLine     = "//"
            , Token.identStart      = letter
            , Token.identLetter     = alphaNum <|> oneOf "_-+'\";%#"
            , Token.reservedNames   = [ "true", "false"]
            , Token.reservedOpNames = ["!", "?", "=>", "<="
                                      , "<=>", "&", "|", "~", ":"
                                      ]
            }

lexer = Token.makeTokenParser languageDef

identifier = Token.identifier lexer -- parses an identifier
reserved   = Token.reserved   lexer -- parses a reserved name
reservedOp = Token.reservedOp lexer -- parses an operator
parens     = Token.parens     lexer -- parses surrounding parenthesis:
                                    --   parens p
                                    -- takes care of the parenthesis and
                                    -- uses p to parse what's inside them
integer    = Token.integer    lexer -- parses an integer
semi       = Token.semi       lexer -- parses a semicolon
comma      = Token.comma      lexer -- parses a comma
dot        = Token.dot        lexer -- parses a dot
whiteSpace = Token.whiteSpace lexer -- parses whitespace
lexeme     = Token.lexeme
