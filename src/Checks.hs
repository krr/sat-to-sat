module Checks where

import Formulas
import Data.Set


checkFormula :: Formula -> Bool
checkFormula f = checkForFreeVars f && checkSOQuantOccurence f

-----------------------------------
--   Check that formula has no free variables (except input-vars)
-----------------------------------


checkForFreeVars :: Formula -> Bool
checkForFreeVars f
    | check f empty = True
    | otherwise = error "Theory contains free symbols"

check :: Formula -> Set Var -> Bool
check (Atom v l) s = checkvar s v && all (checkvar s) l
check (Bool b f g) s = check f s && check g s
check (Neg f) s = check f s
check TrueF s = True
check FalseF s = True
check (Quant q v f) s = check f $ insert v s


checkvar :: Set Var -> Var -> Bool
checkvar s v
    | member v s = True
    | isInputVar v = True
    | otherwise = error ("Free occurence of symbol " ++ show v)


-----------------------------------
--   Check that all SO quantifications (of the form \not \exists SSS) occur in a
--   conjunctive context from the above quantifications
-----------------------------------

--Top level check: formula of the form ? p: ?q : ?s : ...
checkSOQuantOccurence :: Formula -> Bool
checkSOQuantOccurence (Quant E v f)
   | isFOVar v = checkSOQuantConj f
   | otherwise = checkSOQuantOccurence f
checkSOQuantOccurence f = checkSOQuantConj f

--Next level: conjunction in which SO quants are allowed
checkSOQuantConj :: Formula -> Bool
checkSOQuantConj (Bool C f g) = checkSOQuantConj  f && checkSOQuantConj g
checkSOQuantConj (Neg (Quant E v f))
   | isFOVar v = checkNoSOQuant f
   | otherwise = checkSOQuantOccurence f
checkSOQuantConj f = checkNoSOQuant f


checkNoSOQuant :: Formula -> Bool
checkNoSOQuant (Quant q v f)
    | isFOVar v = checkNoSOQuant f
    | otherwise = error ("Quantification over variable " ++ show v ++ "does not occur in a conjunctive context in upper level")
checkNoSOQuant (Bool o f g) = checkNoSOQuant f && checkNoSOQuant g
checkNoSOQuant (Neg f) = checkNoSOQuant f
checkNoSOQuant f = True


-- TODO CHECK THAT SO VARIABLES OCCUR ON SO POSITIONS AND FO VARIABLES ON FO POSITIONS
