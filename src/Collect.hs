module Collect where

import Formulas
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Tuple


collectInputPredicates:: Formula -> [(Var, Int)]
collectInputPredicates f = Set.toList $ cIPInternal f Set.empty


cIPInternal:: Formula -> Set (Var, Int) -> Set (Var, Int)
cIPInternal TrueF s = s
cIPInternal FalseF s = s
cIPInternal (Quant _ _ f) s = cIPInternal f s
cIPInternal (Bool _ f g) s = Set.union (cIPInternal f s) (cIPInternal g s)
cIPInternal (Neg f) s = cIPInternal f s
cIPInternal (Atom a l ) s
    | isInputVar a = Set.insert (a, length l) s
    | otherwise = s
