module Main where
  
import Parser
import TheoryTransformations
import PrintGringo
import Checks

import System.Environment


main :: IO ()
main = do
    args <- getArgs
    let (file, dir) = readArgs args
    --putStrLn file
    ast <- parseFile file
    --putStrLn "Parsed:"
    --putStrLn $ show ast
    let astnew = normalise ast
    --putStrLn "Normalised:"
    --putStrLn $ show astnew
    if checkFormula astnew then printGringo astnew dir else error "ERROR: stopped because of errors"
    --putStrLn "Done"


readArgs :: [String] -> (String, String)
readArgs [] = error "\n\nERROR: usage: so-sat2sat file dir\nwhere\n    * file is a file containing a second order specification\n    * dir is the directory in which so-sat2sat should put its output\n"
readArgs [_] = error "\n\nERROR: usage: so-sat2sat file dir\nwhere\n    * file is a file containing a second order specification\n    * dir is the directory in which so-sat2sat should put its output\n"
readArgs(one:(two:rest)) = (one,two)
