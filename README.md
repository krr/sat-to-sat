To compile simply execute `make`

To run, run
./so-sat2sat arg1 arg2
where
arg1 is a file containing a second-order logic specification
arg2 is a directory in which the tool should output its lp files. Works best if arg2 is an absolute path


For example
./so-sat2sat instances/grounded_extension.SO ~/tmp
creates a file
0000.lp in ~/tmp containing the top specification and linking to subspecifications
and a file
run.sh which is a translator that accepts instances for the given SO specification and turns them into instances for sat2sat
